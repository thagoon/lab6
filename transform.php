<?php

        $xsl_filename = "SummaryBlogs.xsl";
        $xml_filename = "SummaryBlogs.xml";
        $doc = new DOMDocument();
        $xsl = new XSLTProcessor();

        $doc->load($xsl_filename);
        $xsl->importStyleSheet($doc);
        $doc->load($xml_filename);
        
        echo $xsl->transformToXML($doc);
        
?>
